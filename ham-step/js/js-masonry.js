const grid = document.querySelector('.grid');

imagesLoaded( grid, function() {
    const msnry = new Masonry( grid, {
    itemSelector: '.grid-item',
    columnWidth: 1,
      });
});

const grid2 = document.querySelector('.grid2');

let button4 = document.querySelector('.btn4');

button4.addEventListener('click', function () {
    button4.classList.remove('show');
    document.querySelector('.preload2').classList.remove('invisible');
    setTimeout(function (){
        document.querySelector('.preload2').classList.add('invisible');
        grid2.classList.remove('load');
        imagesLoaded( grid2, function() {
            const msnry = new Masonry( grid2, {
                itemSelector: '.grid-item',
                columnWidth: 1,
            });
        });
    }, 6000);
});


