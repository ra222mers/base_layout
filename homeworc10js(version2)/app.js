"use strict";

const buttonConfirm = document.querySelector(".btn");
const passwordForm = document.querySelector(".password-form");

passwordForm.addEventListener("click",function(e) {
  let firstInputValue = document.querySelector(".passwordOne").value;
  let secondInputValue = document.querySelector(".passwordTwo").value;
  let change = document.querySelector(".change");

  if (e.target.classList.contains("fa-eye-slash")) {
    e.target.classList.replace("fa-eye-slash","fa-eye");
    e.target.parentNode.childNodes[1].type = "text";
  } else if (e.target.classList.contains("fa-eye")) {
    e.target.classList.replace("fa-eye","fa-eye-slash");
    e.target.parentNode.childNodes[1].type = "password";
  }

  if (e.target.classList.contains("btn")) {
    e.preventDefault();
    if (firstInputValue === secondInputValue) {
      if (change.style.display === "block") {
        change.style.display = "none";
      }
      alert("You are welcom!");
    } else {
      change.style.display = "block"
    }
  }
});