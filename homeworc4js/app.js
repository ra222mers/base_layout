function createNewUser() {
  this.userName = prompt("Введите ваше имя: ", "");
  while (this.userName === "") {
    this.userName = prompt("Введите ваше имя еще раз: ", "");
  }

  this.userSecondName = prompt("Введите вашу фамилию", "");
  while (this.userSecondName === "") {
    this.userSecondName = prompt("Введите вашу фамилию еще раз: ", "");
  }

  this.getLogin = function () {
    let newLogin =
      this.userName.charAt(0).toLowerCase() + this.userSecondName.toLowerCase();
    return newLogin;
  };
}

let newUser = new createNewUser();
console.log(`Your login is: ${newUser.getLogin()}`);
