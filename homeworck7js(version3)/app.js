"use strict";

const newArray = [
  "hello",
  "world",
  "Kiev",
  "Kharkiv",
  "Odessa",
  "Lviv",
  "sea",
  "user",
  23,
];

const showList = (arr, dbody = document.body) => {
  let li = arr.map((e) => `<li>${e}</li>`);
  let clearLi = li.join("");
  let ul = document.createElement("ul");
  ul.innerHTML = clearLi;
  dbody.prepend(ul);
};

showList(newArray);
