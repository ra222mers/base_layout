let button = document.querySelector(".button");
button.addEventListener("click", () => {
    document.body.classList.contains('item') ? document.body.classList.replace('item', 'color') : document.body.classList.replace('color', 'item');
    localStorage.theme = document.body.className || `item`
});
if (!localStorage.theme) localStorage.theme = `item`;
document.body.className = localStorage.theme ;
