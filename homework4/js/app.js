"use strict";
let button = $(".fixed_button");
let nav = $(".page_nav");
$(document).ready(function () {
  button.on("click", returnToHeader);
  nav.on("click", "a", slowScroll);
  $(document).on("scroll", pageScroll);
  $(document).on("click", "#hide_button", slideToggle);
});
function slowScroll(event) {
  event.preventDefault();
  let id = $(event.target).attr("href");
  let top = $(id).offset().top;
  $("body,html").animate({ scrollTop: top }, 1500);
}
function returnToHeader() {
  $("html, body").animate({ scrollTop: 0 }, 1500);
}
function pageScroll() {
  if ($(window).scrollTop() > $(window).height()) {
    button.show();
  } else {
    button.hide();
  }
}
function slideToggle() {
  if (!$("#hide_button").hasClass("button-active")) {
    $(".top-rated-container").toggle("2000");
    $("#hide_button").addClass("button-active");
  } else {
    $("#hide_button").removeClass("button-active");
    $(".top-rated-container").toggle("2000");
  }
}
